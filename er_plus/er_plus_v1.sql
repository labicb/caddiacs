/* er_plus.sql

Ce script MySQL permet de créer, gérer et alimenter
la base de données er_plus, contenant des données
spectroscopiques sur les transitions de l'ion Er+.

Dans les blocs de création des tableaux, des exemples
d'attributs sont donnés en commentaires.
*/

#SET GLOBAL local_infile = 'ON';

drop database if exists er_plus;
create database if not exists er_plus;
USE er_plus;

drop table if exists transition;
drop table if exists multipole;
drop table if exists level;
drop table if exists config;

create table config (
  configID int NOT NULL,
  configName varchar(32),       # 4f12.6s.6p
  configName_html varchar(64),  # 4f<sup>12</sup>
  PRIMARY KEY (configID)
);

create table level (
  levelID int primary key,
  energyExp decimal(9,3),          # in cm-1. May be unknown
  energyTh  decimal(7,1) not null, # in cm-1.
  parity int not null,             # +1 or -1
  config int not null,
    foreign key (config) references config(configID),
  configAndTerm varchar(256) not null, # 4f12(3H6).6s6p(3P1*) (6,1)*
  configAndTerm_html varchar(256) not null, # 4f<sup>11</sup> <sup>4</sup>I<sup>o</sup><sub>15/2</sub>
  leadPrct decimal(4,1) not null,   # ex: 85.6 (%)
  angMomJ decimal(3,1) not null,    # 6.0 or 7.5
  landeGExp decimal(4,3),           # 1.115, may be unknown
  landeGTh  decimal(4,3) not null
);

create table multipole (
  multipoleID int not null,  # 1 for E1, 2 for M1
  multipoleType varchar(2) not null,
  primary key (multipoleID)
);

create table transition (
  transitionID int primary key,
  lowLev int not null,
    foreign key (lowLev) references level(levelID),
  uppLev int not null,
    foreign key (uppLev) references level(levelID),
  multipType int not null,
    foreign key (multipType) references multipole(multipoleID),
  waveLenVacExp decimal(9,3),  # in nm, may be unknown 
  waveLenVacTh  decimal(7,1) not null,  # in nm  
  waveNumVacExp decimal(9,3),  # in cm-1, may be unknown
  waveNumVacTh  decimal(7,1) not null,  # in cm-1
  oscStrAbsTh float not null,  # unitless
  einsteinATh float not null   # in s-1
);


# LOAD DATA FROM EXTERNAL FILES

insert into multipole values ( 1, "E1" );
insert into multipole values ( 2, "M1" );

#load data infile "C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/Er+_configs.txt"
load data local infile "Er+_configs.txt"
   replace into table config fields terminated by "\t";


#load data infile "C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/Er+_levels.txt"
load data local infile "Er+_levels.txt"
   replace into table level fields terminated by "\t"
;

#truncate table transition; # to reset the table
#load data infile "C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/Er+_transitions.txt"
load data local infile "Er+_transitions.txt"
  replace into table transition fields terminated by "\t"
;
