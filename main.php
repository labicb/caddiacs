<!--  
    File "caddiacs/main.php"
    containing the search form of the CaDDiAcS database

    SMPCA Team, Lab. ICB, Dijon, 15/10/2021
-->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="CaDDiAcS Search Form">
    <meta name="keywords" content="Database, spectra, atoms, transitions, intensities">
    <meta name="author" content="SMPCA Team, Lab ICB, CNRS, University of Burgundy, Dijon, France">

    <title>CaDDiAcS - Calculated Database of Dijon for Atomic Spectra</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="search_form.css">
    <link rel="icon" type="image/png" href="/PHP/cnrs.png">
    <link rel="apple-touch-icon" href="/PHP/cnrs.png">

    <script type="text/javascript">
      var clck_wave = 0, clck_aik = 0, clck_fik = 0,
          clck_uppener = 0, clck_lowener = 0, clck_trstype = 0 ;
      $(document).ready(function (){
        console.log('adad');
      /*
        Function handling the "+ criteria" addButton
      */
      $("#addButton").click(function(){
        var val = document.getElementById('firstopt').value;
        document.getElementById('options').innerHTML += (val !== 'Wavelength_in_Vacuum' ? '<label> <input name="wave" id="wave" value="wave" onclick="optionChoose(this)" type="checkbox"> Vacuum Wavelength (nm)</label> <br>' : "");
        document.getElementById('options').innerHTML += (val !== 'A_ki' ? '<label> <input name="A_ki" id="aki" value="aki" onclick="optionChoose(this)" type="checkbox"> Einstein A coefficient (s<sup>-1</sup>) </label> <br>' : "") ;
        document.getElementById('options').innerHTML += (val !== 'f_ik' ? '<label> <input name="f_ik" id="fik" value="fik" onclick="optionChoose(this)" type="checkbox"> Absorption Oscillator Strength <sup>&nbsp; </sup> </label> <br>' : "") ;
        document.getElementById('options').innerHTML += (val !== 'Low_Ener' ? '<label> <input name="lowener" value="lowener" id="lowener" onclick="optionChoose(this)" type="checkbox"> Lower Level Energy (cm<sup>-1</sup>) </label> <br>' : "") ;
        document.getElementById('options').innerHTML += (val !== 'Upp_Ener' ? '<label> <input name="uppener" value="uppener" id="uppener" onclick="optionChoose(this)" type="checkbox"> Upper Level Energy (cm<sup>-1</sup>) </label> <br>' : "") ;
        document.getElementById('options').innerHTML += '<label> <input name="trstype" value="trstype" id="trstype" onclick="optionChoose(this)" type="checkbox"> Transition Type <sup>&nbsp;</sup> </label> <br>' ;

        // Hide '+ Criteria' button
        $("#addButton").css('display', 'none') ;
        })
      });

      /*
        When changing first option, reset the "+ criteria" button
      */
      function chgFirstOpt() {
        document.getElementById("options").innerHTML = "" ;
        document.getElementById("addButton").style.display = "inline" ;
      }

      /*
        When choosing a criterion, displays
        the "from" and "to" boxes.
      */
      function optionChoose(elem){
        switch (elem.value) {
          case 'wave':
            clck_wave++ ;
            if(!$('#wave').is(":checked")){
              document.getElementById('fromgroupwave').style.display = 'none' ;
              document.getElementById("fromwave").value = "" ;
              document.getElementById("towave").value = "" ;
              return ;
            }
            else{
              if ( clck_wave == 1 ) {
                document.getElementById('wave').parentElement.innerHTML += '<div class="from" id="fromgroupwave"> <input id="fromwave" name="fromwave" type="text" placeholder="FROM"> <input id="towave" name="towave" type="text" placeholder="TO"> </div>' ;
              }
              else {
                document.getElementById('fromgroupwave').style.display = 'block' ;
              }
              $('#wave').prop('checked', true) ;
            }
            break;
          case 'aki':
            clck_aik++ ;
            if(!$('#aki').is(":checked")){
//              $('#aki').prop('checked', true);
              document.getElementById('fromgroupaki').style.display = 'none' ;
              document.getElementById("fromaki").value = "" ;
              document.getElementById("toaki").value = "" ;
              return ;
            }
            else{
              if ( clck_aik == 1 ) {
                document.getElementById('aki').parentElement.innerHTML += '<div class="from" id="fromgroupaki"> <input id="fromaki" name="fromaki" type="text" placeholder="FROM"> <input id="toaki" name="toaki" type="text" placeholder="TO"> </div>' ;
              }
              else {
                document.getElementById('fromgroupaki').style.display = 'block' ;
              }
              $('#aki').prop('checked', true);
            }
            break ;
          case 'fik':
            clck_fik++ ;
            if(!$('#fik').is(":checked")){
              document.getElementById('fromgroupfik').style.display = 'none' ;
              document.getElementById("fromfik").value = "" ;
              document.getElementById("tofik").value = "" ;
              return ;
            }
            else{
              if ( clck_fik == 1 ) {
                document.getElementById('fik').parentElement.innerHTML += '<div class="from" id="fromgroupfik"> <input id="fromfik" name="fromfik" type="text" placeholder="FROM"> <input id="tofik" name="tofik" type="text" placeholder="TO"> </div>' ;
              }
              else {
                document.getElementById('fromgroupfik').style.display = 'block' ;
              }
              $('#fik').prop('checked', true) ;
            }
            break;
          case 'uppener':
            clck_uppener++ ;
            if(!$('#uppener').is(":checked")){
              document.getElementById('fromgroupuppener').style.display = 'none' ;
              document.getElementById("fromuppener").value = "" ;
              document.getElementById("touppener").value = "" ;
              return ;
            }
            else{
              if ( clck_uppener == 1 ) {
                document.getElementById('uppener').parentElement.innerHTML += '<div class="from" id="fromgroupuppener"> <input id="fromuppener" name="fromuppener" type="text" placeholder="FROM"> <input id="touppener" name="touppener" type="text" placeholder="TO"> </div>' ;
              }
              else {
                document.getElementById('fromgroupuppener').style.display = 'block' ;
              }
              $('#uppener').prop('checked', true) ;
            }
            break;
          case 'lowener':
            clck_lowener++ ;
            if(!$('#lowener').is(":checked")){
              document.getElementById('fromgrouplowener').style.display = 'none' ;
              document.getElementById("fromlowener").value = "" ;
              document.getElementById("tolowener").value = "" ;
              return ;
            }
            else{
              if ( clck_lowener == 1 ) {
                document.getElementById('lowener').parentElement.innerHTML += '<div class="from" id="fromgrouplowener"> <input id="fromlowener" name="fromlowener" type="text" placeholder="FROM"> <input id="tolowener" name="tolowener" type="text" placeholder="TO"> </div>' ;
              }
              else {
                document.getElementById('fromgrouplowener').style.display = 'block' ;
              }
              $('#lowener').prop('checked', true) ;
            }
            break;
          case 'trstype':
            clck_trstype++ ;
            if(!$('#trstype').is(":checked")){
              document.getElementById('grouptrstype').style.display = 'none' ;
              return ;
            }
            else{
              if ( clck_trstype == 1 ) {
                document.getElementById('trstype').parentElement.innerHTML += '<div id="grouptrstype"> &emsp; <label> <input name="oute1" value="oute1" type="checkbox" checked> Electric dipole (E1) <sup>&nbsp; </sup> </label> <sup>&nbsp;</sup> <br> &emsp; <label> <input name="outm1" value="outm1" type="checkbox"> Magnetic dipole (M1) </label> <sup>&nbsp;</sup> </div>' ;
              }
              else {
                document.getElementById('grouptrstype').style.display = 'block' ;
              }
              $('#trstype').prop('checked', true) ;
            }
            break;
          default:
            break;
        }
      }

      /*
        Manages the change of output format.
      */
      function chgOutForm() {
        if ( $('#outformhtml').is(":checked") ) {
          $('#outterm').prop('checked', true) ;
          $('#outpar').prop('checked', false) ;
        }
        if ( $('#outformtxt').is(":checked") ) {
          $('#outterm').prop('checked', false) ;
          $('#outpar').prop('checked', true) ;
        }
      }
    </script>
  </head>
  <body>
    <h1> Welcome to the Search Form <br> of the CaDDiAcS database </h1>
    <form action="database.php" method="POST">
      <!--
            Select atomic species */
      -->
      <label for="species"> Select species</lael><br>
      <select id="species" name="species">
        <option value="Er+" selected> Er<sup>+</sup> (Er II) </option>
<!--        <option value="Er"> Er (Er I) </option> -->
      </select>
      <!--
            Select first criterion */
      -->
      <p> </p>
      <div class="row">
        <!--
            Select first criterion */
        -->
        <label for="firstopt"> Select seach criterion</lael><br>
        <select id="firstopt" name="table" onchange="chgFirstOpt()" required>
          <option value="Wavelength_in_Vacuum">Vacuum Wavelength (nm)</option>
          <option value="A_ki">Einstein A coef. (s<sup>-1</sup>)</option>
          <option value="f_ik">Absorption Osc. Str.</option>
          <option value="Low_Ener">Lower Level Energy (cm<sup>-1</sup>)</option>
          <option value="Upp_Ener">Upper Level Energy (cm<sup>-1</sup>)</option>
        </select>
        <div class="from" id="fromgroup">
          <input id="from" name="from" type="text" placeholder="FROM">
          <input id="to" name="to" type="text" placeholder="TO">
        </div>
        <!--
            Select additional criteria */
        -->
        <br>
        <div id="options">
        </div>
        <div>
          <button type="button" id="addButton">+ Criteria</button>
        </div>

        <!--
            Output options
        -->
        <h2>Output options </h2>
        <b>Show transition properties</b> <br>
        <div id="outtrans">
          <label> <input name="outwave" value="outwave" type="checkbox" checked> Vacuum Wavelength (nm) <sup>&nbsp; </sup> </label> <br>
          <label> <input name="outaik" value="outaik" type="checkbox" checked> Einstein A coefficient (s<sup>-1</sup>) </label> <br>
          <label> <input name="outfik" value="outfik" type="checkbox">  Absorption Oscillator Strength <sup>&nbsp; </sup> </label> <br>
        </div>
        <br>
        <b>Show level properties</b> <br>
        <div id="outlev">
          <label> <input name="outener" value="outener" type="checkbox" checked> Energy (cm<sup>-1</sup>) </label>
          <label> <input name="outgl" value="outgl" type="checkbox"> Land&eacute; g-factor <sup>&nbsp;</sup> </label> <br>
          <label> <input name="outj" value="outj" type="checkbox" checked> Total angular momentum J <sup>&nbsp; </sup> </label> <br>
          <label> <input name="outconf" value="outconf" type="checkbox"> Leading configuration <sup>&nbsp; </sup> </label>
          <label> <input id="outpar" name="outpar" value="outpar" type="checkbox"> Parity <sup>&nbsp; </sup> </label> <br>
          <label> <input id="outterm" name="outterm" value="outterm" type="checkbox" checked> Leading config. and Term <sup>&nbsp; </sup> </label> <br>
        </div>

        <!--
            Sorting options
        -->
        <br>
        <b>Sorting options</b> <br>
        <select name="sort-qty" required>
          <option value="Wavelength_in_Vacuum">Vacuum Wavelength</option>
          <option value="A_ki">Einstein A coefficient</option>
          <option value="f_ik">Absorption Oscillator Strength</option>
          <option value="Lower_Level_Energy">Lower Level Energy</option>
          <option value="Upper_Level_Energy">Upper Level Energy</option>
        </select><br>
        <label> <input type="radio" name="sort-ord" value="inc" checked> Increasing order </label> &nbsp;
        <label> <input type="radio" name="sort-ord" value="dec"> Decreasing order </label>
        <br>

        <!--
            Output Format
        -->
        <br>
        <b>Output Format</b> <br>
        <label> <input type="radio" name="outform" value="html" id="outformhtml" onchange="chgOutForm()" checked> HTML </label> &nbsp;
        <label> <input type="radio" name="outform" value="txt"  id="outformtxt"  onchange="chgOutForm()"> Text </label>
        <br>

        <button type="reset" style="margin: 40px 10px">Reset</button>
        <button type="submit" style="margin: 40px">Get Data</button>
      </div>
    </form>

    <!--
        Footer with logos
    -->
    <div id="footer">
        <a target="_blank" href=http://www.cnrs.fr/>
        <img style='background-color: #dddddd;' src="/PHP/cnrs.png" alt="CNRS logo" height="50"></a>
        &nbsp;
        <a target="_blank" href=http://dataosu.obs-besancon.fr/>
        <img style='background-color: #dddddd;' src="/PHP/dataOSU.png" alt="Data OSU logo" height="50"></a>
        &nbsp;
        <a target="_blank" href=http://icb.u-bourgogne.fr/>
        <img style='background-color: #dddddd;' src="/PHP/icb.png" alt="ICB logo" height="50"></a>
        &nbsp;
        <a target="_blank" href=https://www.u-bourgogne.fr/>
        <img style='background-color: #dddddd;' src="/PHP/ub.png" alt="UB logo" height="50"></a>
    </div>
  </body>
</html>
