<!--  
    File "caddiacs/database.php"
    managing database request and displaying output.

    SMPCA Team, Lab. ICB, Dijon, 15/10/2021
-->

<?php
  // Selects database names
  if ( $_POST['species'] == 'Er+' ) {
    $selected_db = 'er_plus' ;
  }
  else {
     die ("ERROR : no database available for ".$_POST['species']) ;
  }

  /*
     Below, the code searches for a file containing
     notes about the data (reference, warning, ...).
     The file is located in a folder whose
     name is the database name (ex: "er_plus").
     The file name is the species + a pattern given below
     (ex: "Er+" + "_notes.txt").
  */
  $ptrn_file_note = "_notes.txt" ;

  // Connects to database
  require ('login.php') ;
  $conn = new mysqli($servername, $username, $password, $selected_db);
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

  // Table and attribute names in the MySQL database
  $nb_row_max  = 1000 ; // max result nber
  $tab_trans   = "transition" ;
  $tab_level   = "level" ;
  $tab_config  = "config" ;
  $trs_wvl_exp = "waveLenVacExp" ;
  $trs_wvl_th  = "waveLenVacTh" ;
  $trs_aki_th  = "einsteinATh" ;
  $trs_fik_th  = "oscStrAbsTh" ;
  $trs_typ     = "multipType" ;
  $trs_low_lev = "lowLev" ;
  $trs_upp_lev = "uppLev" ;
  $lev_enr_exp = "energyExp" ;
  $lev_enr_th  = "energyTh" ;
  $lev_gl_exp = "landeGExp" ;
  $lev_gl_th  = "landeGTh" ;
  $lev_par     = "parity" ;
  $lev_j       = "angMomJ" ;
  $lev_cfg     = "config" ;
  $lev_trm_txt = "configAndTerm" ;
  $lev_trm_htm = "configAndTerm_html" ;
  $lev_trm_prc = "leadPrct" ;
  $cfg_nam_txt = "configName" ;
  $cfg_nam_htm = "configName_html" ;
  $levs_in_trs = array("l1","l2") ;
  $cfgs_in_trs = array("c1","c2") ;

  // Labels for table headers
  $lab_qty = array("outwave" => "Wavelength (nm)"
           , "outaik" => "Einstein A coeff. (s<sup>-1</sup>)"
           , "outfik" => "Absorption Oscillator Stength"
           , "outener" => "Energy (cm<sup>-1</sup>)"
           , "outgl" => "Land&eacute; g-factor"
           , "outj" => "Angular Momentum"
           , "outpar" => "Parity"
           , "outconf" => "Leading Configuration"
           , "outterm" => "Leading Term"
           , "outprct" => "Leading Percentage"
           ) ;
  $lab_lev = array( "Lower", "Upper" ) ;
  $lab_par = array("-1" => "odd", "1" => "even" ) ;

  // Labels for table headers
  $form_qty = array("outwave" => "%10.3f"
            , "outaik" => "%10.3e"
            , "outfik" => "%10.3e"
            , "outener" => "%11.3f"
            , "outgl" => "%7.3f"
            , "outj" => "%6.1f"
            , "outpar" => "%6s"
            , "outconf" => " %-32s"
            , "outterm" => " %-96s"
            , "outprct" => "%6.1f"
            ) ;


  /* ------------------
       Some functions
     ------------------ */

  function print_j (float $jval): string {
    if ( (2.*$jval)%2 == 0 ) {
      return sprintf ("%d",$jval) ;
    }
    else {
      return sprintf ("%d/2",2.*$jval) ;
    }
  }

  // For some quantites like level energies,
  // prints experimental value if exists,
  // or theoretical one otherwise,
  // with a special style
  function print_exp_th_html ( $qty_exp, $qty_th ) {
    if ( $qty_exp != "" ) {
      echo "<td>".$qty_exp."</td>" ;
    }
    else {
      echo '<td class="theo">'.$qty_th."</td>" ;
    }
  }
  function print_exp_th_txt ( $forma, $qty_exp, $qty_th ) {
    if ( $qty_exp != "" ) {
      printf ( ($forma.' E'), $qty_exp ) ;
    }
    else {
      printf ( ($forma.' T'), $qty_th ) ;
    }
  }


  /* -----------------------
       Gathers the search criteria, to build
       the "where" block of the sql request.
     -----------------------  */

  // Tag = 0 if no filter, <>0 otherwise
  $i_where = 0 ;
  $sql2 = " " ;

  // Wavelength filters
  $test_crit = ( ($_POST["table"]=="Wavelength_in_Vacuum")
                && isset($_POST["from"]) ) ;
  if ( $test_crit ) {
    if ($_POST["from"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . " ( " . $tab_trans.".".$trs_wvl_exp . " >= " . $_POST["from"] . " or ( " . $tab_trans.".".$trs_wvl_exp." is null and " . $tab_trans.".".$trs_wvl_th . " >= ".$_POST["from"] . " ) )" ;
    }
  }
  $test_crit = ( ($_POST["table"]=="Wavelength_in_Vacuum")
                && isset($_POST["to"]) ) ;
   if ( $test_crit ) {
    if ($_POST["to"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . " ( " . $tab_trans.".".$trs_wvl_exp . " <= " . $_POST["to"] . " or ( " . $tab_trans.".".$trs_wvl_exp." is null and " . $tab_trans.".".$trs_wvl_th . " <= ".$_POST["to"] . " ) )" ;
    }
  }
  $test_crit = isset($_POST["fromwave"]) ;
  if ( $test_crit ) {
    if ($_POST["fromwave"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . " ( " . $tab_trans.".".$trs_wvl_exp . " >= " . $_POST["fromwave"] . " or ( " . $tab_trans.".".$trs_wvl_exp." is null and " . $tab_trans.".".$trs_wvl_th . " >= ".$_POST["fromwave"] . " ) )" ;
    }
  }
  $test_crit = isset($_POST["towave"]) ;
  if ( $test_crit ) {
    if ($_POST["towave"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . " ( " . $tab_trans.".".$trs_wvl_exp . " <= " . $_POST["towave"] . " or ( " . $tab_trans.".".$trs_wvl_exp." is null and " . $tab_trans.".".$trs_wvl_th . " <= ".$_POST["towave"] . " ) )" ;
    }
  }

  // Enistein coefficient filters
  $test_crit = ( ($_POST["table"]=="A_ki")
                && isset($_POST["from"]) ) ;
  if ( $test_crit ) {
    if ($_POST["from"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . $tab_trans.".".$trs_aki_th . " >= " . $_POST["from"] ;
    }
  }
  $test_crit = ( ($_POST["table"]=="A_ki")
                && isset($_POST["to"]) ) ;
  if ( $test_crit ) {
    if ($_POST["to"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . $tab_trans.".".$trs_aki_th . " <= " . $_POST["to"] ;
    }
  }
  $test_crit = isset($_POST["fromaki"]) ;
  if ( $test_crit ) {
    if ($_POST["fromaki"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . $tab_trans.".".$trs_aki_th . " >= " . $_POST["fromaki"] ;
    }
  }
  $test_crit = isset($_POST["toaki"]) ;
  if ( $test_crit ) {
    if ($_POST["toaki"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . $tab_trans.".".$trs_aki_th . " <= " . $_POST["toaki"] ;
    }
  }

  // Oscillator Strength filters
  $test_crit = ( ($_POST["table"]=="f_ik")
                && isset($_POST["from"]) ) ;
  if ( $test_crit ) {
    if ($_POST["from"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . $tab_trans.".".$trs_fik_th . " >= " . $_POST["from"] ;
    }
  }
  $test_crit = ( ($_POST["table"]=="f_ik")
                && isset($_POST["to"]) ) ;
  if ( $test_crit ) {
    if ($_POST["to"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . $tab_trans.".".$trs_fik_th . " <= " . $_POST["to"] ;
    }
  }
  $test_crit = isset($_POST["fromfik"]) ;
  if ( $test_crit ) {
    if ($_POST["fromfik"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . $tab_trans.".".$trs_fik_th . " >= " . $_POST["fromfik"] ;
    }
  }
  $test_crit = isset($_POST["tofik"]) ;
  if ( $test_crit ) {
    if ($_POST["tofik"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . $tab_trans.".".$trs_fik_th . " <= " . $_POST["tofik"] ;
    }
  }

  // Lower energy filters
  $test_crit = ( ($_POST["table"]=="Low_Ener")
                && isset($_POST["from"]) ) ;
  if ( $test_crit ) {
    if ($_POST["from"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . " ( " . $levs_in_trs[0].".".$lev_enr_exp . " >= " . $_POST["from"] . " or ( " . $levs_in_trs[0].".".$lev_enr_exp." is null and " . $levs_in_trs[0].".".$lev_enr_th . " >= ".$_POST["from"] . " ) )" ;
    }
  }
  $test_crit = ( ($_POST["table"]=="Low_Ener")
                && isset($_POST["to"]) ) ;
  if ( $test_crit ) {
    if ($_POST["to"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . " ( " . $levs_in_trs[0].".".$lev_enr_exp . " <= " . $_POST["to"] . " or ( " . $levs_in_trs[0].".".$lev_enr_exp." is null and " . $levs_in_trs[0].".".$lev_enr_th . " <= ".$_POST["to"] . " ) )" ;
    }
  }
  $test_crit = isset($_POST["fromlowener"]) ;
  if ( $test_crit ) {
    if ($_POST["fromlowener"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . " ( " . $levs_in_trs[0].".".$lev_enr_exp . " >= " . $_POST["fromlowener"] . " or ( " . $levs_in_trs[0].".".$lev_enr_exp." is null and " . $levs_in_trs[0].".".$lev_enr_th . " >= ".$_POST["fromlowener"] . " ) )" ;
    }
  }
  $test_crit = isset($_POST["tolowener"]) ;
  if ( $test_crit ) {
    if ($_POST["tolowener"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . " ( " . $levs_in_trs[0].".".$lev_enr_exp . " <= " . $_POST["tolowener"] . " or ( " . $levs_in_trs[0].".".$lev_enr_exp." is null and " . $levs_in_trs[0].".".$lev_enr_th . " <= ".$_POST["tolowener"] . " ) )" ;
    }
  }

  // Upper energy filters
  $test_crit = ( ($_POST["table"]=="Upp_Ener")
                && isset($_POST["from"]) ) ;
  if ( $test_crit ) {
    if ($_POST["from"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . " ( " . $levs_in_trs[1].".".$lev_enr_exp . " >= " . $_POST["from"] . " or ( " . $levs_in_trs[1].".".$lev_enr_exp." is null and " . $levs_in_trs[1].".".$lev_enr_th . " >= ".$_POST["from"] . " ) )" ;
    }
  }
  $test_crit = ( ($_POST["table"]=="Upp_Ener")
                && isset($_POST["to"]) ) ;
  if ( $test_crit ) {
    if ($_POST["to"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . " ( " . $levs_in_trs[1].".".$lev_enr_exp . " <= " . $_POST["to"] . " or ( " . $levs_in_trs[1].".".$lev_enr_exp." is null and " . $levs_in_trs[1].".".$lev_enr_th . " <= ".$_POST["to"] . " ) )" ;
    }
  }
  $test_crit = isset($_POST["fromuppener"]) ;
  if ( $test_crit ) {
    if ($_POST["fromuppener"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . " ( " . $levs_in_trs[1].".".$lev_enr_exp . " >= " . $_POST["fromuppener"] . " or ( " . $levs_in_trs[1].".".$lev_enr_exp." is null and " . $levs_in_trs[1].".".$lev_enr_th . " >= ".$_POST["fromuppener"] . " ) )" ;
    }
  }
  $test_crit = isset($_POST["touppener"]) ;
  if ( $test_crit ) {
    if ($_POST["touppener"] != "") {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . " ( " . $levs_in_trs[1].".".$lev_enr_exp . " <= " . $_POST["touppener"] . " or ( " . $levs_in_trs[1].".".$lev_enr_exp." is null and " . $levs_in_trs[1].".".$lev_enr_th . " <= ".$_POST["touppener"] . " ) )" ;
    }
  }

  // Transition type filters
  $test_crit = isset($_POST["trstype"]) ;
  if ( $test_crit ) {
    if ( isset($_POST["oute1"]) && !isset($_POST["outm1"]) ) {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . " " . $tab_trans.".".$trs_typ . " = 1 " ;
    }
    else if ( !isset($_POST["oute1"]) && isset($_POST["outm1"]) ) {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . " " . $tab_trans.".".$trs_typ . " = 2 " ;
    }
    else if ( isset($_POST["oute1"]) && isset($_POST["outm1"]) ) {
      $i_where++ ;
      if ( $i_where > 1 ) $sql2 .= " and " ;
      $sql2 = $sql2 . " ( " . $tab_trans.".".$trs_typ . " = 1 or "
                            . $tab_trans.".".$trs_typ . " = 2 ) " ;
    }
  }
  else {
    // By default, prints only E1 transitions
    $i_where++ ;
    if ( $i_where > 1 ) $sql2 .= " and " ;
    $sql2 = $sql2 . " " . $tab_trans.".".$trs_typ . " = 1 " ;
  }

  // If at least one condition has been set,
  // prints "where" in the SQL request
  if ( $i_where > 0 ) {
    $sql2 = " where " . $sql2 ;
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="CaDDiAcS Search Output">
  <meta name="keywords" content="Database, spectra, atoms, transitions, intensities">
  <meta name="author" content="SMPCA Team, Lab ICB, CNRS, University of Burgundy, Dijon, France">

  <title>CaDDiAcS - Calculated Database of Dijon for Atomic Spectra</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <link rel="stylesheet" href="search_output.css">
  <link rel="icon" type="image/png" href="/PHP/cnrs.png">
  <link rel="apple-touch-icon" href="/PHP/cnrs.png">

  <script type="text/javascript">
  /*
    Script to save data in text format output
  */
    function saveDataTxt ()
    {
      // get the textbox data...
      let textToWrite =  document.getElementById("outtxt").innerHTML ;
      // Remove all HTML tags from text
      textToWrite = textToWrite.replace(/<pre>/g,"") ;
      textToWrite = textToWrite.replace(/<sup>/g,"") ;
      textToWrite = textToWrite.replace(/<\/sup>/g,"") ;
      textToWrite = textToWrite.replace(/<\/pre>/g,"\n") ;
      // put the data in a Blob object...
      let textFileAsBlob = new Blob([textToWrite], {type:'text/plain'}) ;
      // create a hyperlink <a> element tag that will be automatically clicked below...
      let downloadLink = document.createElement("a") ;
      // set the file name...
      downloadLink.download = "search_output.txt" ;
      // set the <a> tag href as a URL to point to the Blob
      downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob) ;
      // automaitcally click the <a> element to go to the URL to save the textFileAsBlob...
      downloadLink.click() ;
    }
  </script>
</head>
<body>
  <button type="button">
  <a style="text-decoration: none;" href="javascript:history.back()"> Back </a></button>

  <?php
    /*
      Constructs SQL request,
      in particular the "select" part.
    */
    $sql = "select " ;
    $i_select = 0 ;
    // Display transition characteristics
    if ( isset($_POST["outwave"]) ) {
      $i_select++ ;
      if ( $i_select > 1 ) $sql .= ", " ;
      $sql = $sql . $tab_trans.".".$trs_wvl_exp . ", "
                  . $tab_trans.".".$trs_wvl_th ;
    }
    if ( isset($_POST["outaik"]) ) {
      $i_select++ ;
      if ( $i_select > 1 ) $sql .= ", " ;
      $sql = $sql . $tab_trans.".".$trs_aki_th ;
    }
    if ( isset($_POST["outfik"]) ) {
      $i_select++ ;
      if ( $i_select > 1 ) $sql .= ", " ;
      $sql = $sql . $tab_trans.".".$trs_fik_th ;
    }

    // Display level characteristics
    for ( $i=0; $i<2; $i++ ) {
      if ( isset($_POST["outener"]) ) {
        $i_select++ ;
        if ( $i_select > 1 ) $sql .= ", " ;
        $sql = $sql . $levs_in_trs[$i].".".$lev_enr_exp
             . " as " . $levs_in_trs[$i].$lev_enr_exp . " "
             . ", " . $levs_in_trs[$i].".".$lev_enr_th
             . " as " . $levs_in_trs[$i].$lev_enr_th . " "
        ;
      }
      if ( isset($_POST["outgl"]) ) {
        $i_select++ ;
        if ( $i_select > 1 ) $sql .= ", " ;
        $sql = $sql . $levs_in_trs[$i].".".$lev_gl_exp
             . " as " . $levs_in_trs[$i].$lev_gl_exp . " "
             . ", " . $levs_in_trs[$i].".".$lev_gl_th
             . " as " . $levs_in_trs[$i].$lev_gl_th . " "
        ;
      }
      if ( isset($_POST["outj"]) ) {
        $i_select++ ;
        if ( $i_select > 1 ) $sql .= ", " ;
        $sql = $sql . $levs_in_trs[$i].".".$lev_j
             . " as " . $levs_in_trs[$i].$lev_j . " "
        ;
      }
      if ( isset($_POST["outconf"]) ) {
        $i_select++ ;
        if ( $i_select > 1 ) $sql .= ", " ;
        $sql = $sql . $cfgs_in_trs[$i].".".$cfg_nam_txt
             . " as " . $cfgs_in_trs[$i].$cfg_nam_txt . " "
             . ", " . $cfgs_in_trs[$i].".".$cfg_nam_htm
             . " as " . $cfgs_in_trs[$i].$cfg_nam_htm . " "
        ;
      }
      if ( isset($_POST["outpar"]) ) {
        $i_select++ ;
        if ( $i_select > 1 ) $sql .= ", " ;
        $sql = $sql . $levs_in_trs[$i].".".$lev_par
             . " as " . $levs_in_trs[$i].$lev_par . " "
        ;
      }
      if ( isset($_POST["outterm"]) ) {
        $i_select++ ;
        if ( $i_select > 1 ) $sql .= ", " ;
        $sql = $sql . $levs_in_trs[$i].".".$lev_trm_txt
             . " as " . $levs_in_trs[$i].$lev_trm_txt . " "
             . ", " . $levs_in_trs[$i].".".$lev_trm_htm
             . " as " . $levs_in_trs[$i].$lev_trm_htm . " "
             . ", " . $levs_in_trs[$i].".".$lev_trm_prc
             . " as " . $levs_in_trs[$i].$lev_trm_prc . " "
        ;
      }
    }
    $sql .= " from " . $tab_trans ;

    // Inner join statements
    $sql .= " inner join " . $tab_level . " as " . $levs_in_trs[0]
      . " on " . $tab_trans.".".$trs_low_lev
      . " = " . $levs_in_trs[0].".".$tab_level."ID" ;
    $sql .= " inner join " . $tab_config . " as " . $cfgs_in_trs[0]
      . " on " . $levs_in_trs[0].".".$lev_cfg
      . " = " . $cfgs_in_trs[0].".".$tab_config."ID" ;
    $sql .= " inner join " . $tab_level . " as " . $levs_in_trs[1]
      . " on " . $tab_trans.".".$trs_upp_lev
      . " = " . $levs_in_trs[1].".".$tab_level."ID" ;
    $sql .= " inner join " . $tab_config . " as " . $cfgs_in_trs[1]
      . " on " . $levs_in_trs[1].".".$lev_cfg
      . " = " . $cfgs_in_trs[1].".".$tab_config."ID" ;

    /*
       Sorting option
    */
    $sql .= $sql2 . " order by " ; //transition.waveLenVacTh" ;
    switch ($_POST["sort-qty"]) {
     case 'Wavelength_in_Vacuum':
      $sql .= $tab_trans.".".$trs_wvl_th ;
      switch ($_POST["sort-ord"]) {
       case 'inc':
        $sql .= " asc " ;
        break;
       case 'dec':
        $sql .= " desc " ;
        break;
       default:
        die (' ERROR in sorting part !') ;
        break;
      }
      break;
     case 'A_ki':
      $sql .= $tab_trans.".".$trs_aki_th ;
      switch ($_POST["sort-ord"]) {
       case 'inc':
        $sql .= " asc " ;
        break;
       case 'dec':
        $sql .= " desc " ;
        break;
       default:
        die (' ERROR in sorting part !') ;
        break;
      }
      break;
     case 'f_ik':
      $sql .= $tab_trans.".".$trs_fik_th ;
      break;
     case 'Lower_Level_Energy':
      switch ($_POST["sort-ord"]) {
       case 'inc':
        $sql .= $levs_in_trs[0].".".$lev_enr_th . " asc, "
              . $levs_in_trs[1].".".$lev_enr_th . " asc " ;
        break;
       case 'dec':
        $sql .= $levs_in_trs[0].".".$lev_enr_th . " desc, "
              . $levs_in_trs[1].".".$lev_enr_th . " desc " ;
        break;
       default:
        die (' ERROR in sorting part !') ;
        break;
      }
      break;
     case 'Upper_Level_Energy':
      switch ($_POST["sort-ord"]) {
       case 'inc':
        $sql .= $levs_in_trs[1].".".$lev_enr_th . " asc, "
              . $levs_in_trs[0].".".$lev_enr_th . " asc " ;
        break;
       case 'dec':
        $sql .= $levs_in_trs[1].".".$lev_enr_th . " desc, "
              . $levs_in_trs[0].".".$lev_enr_th . " desc " ;
        break;
       default:
        die (' ERROR in sorting part !') ;
        break;
      }
      break;
     default:
      die (' ERROR in sorting part !') ;
      break;
    }

    // Limits the nber of rows in HTML format
    if ( $_POST["outform"] == 'html' ) {
      $sql .= " limit " . $nb_row_max ;
    }
    $sql .= " ;" ;

    /*
       Executes SQL query
    */
//echo ("<p>".$sql."</p>") ;
    $result = mysqli_query($conn, $sql);
    if (!$result) {
      die("Error while executing SQL request !" ) ;
    }

    // Display nber of results
    echo '<div id="numres">' ;
    if ($result->num_rows > 1) {
      echo '<b> &emsp;' . $result->num_rows . ' transitions found.' ;
    }
    else {
      echo "<b> &emsp;".$result->num_rows." transition found." ;
    }
    if ( ( $_POST["outform"] == 'html' )
      && ( $result->num_rows == $nb_row_max ) ) {
      echo " Reached maximum nber of displayed rows." ;
    }
    echo "&emsp; </b>" ;
    echo '</div>' ;
    // For text output, button to save output to file
    if ( $_POST["outform"] == 'txt' ) {
      echo '<button type="button" onclick=saveDataTxt()> Save Data </button>' ;
    }

    /*
        If filz with notes about the results exists,
        displays its content above the output data.
    */
    $fname = ( $selected_db ."/". $_POST['species'] . $ptrn_file_note) ;
    if ( file_exists($fname) )
    {
      $file_note = fopen ( $fname, "r" ) ;
      $note_data = fread($file_note,filesize($fname)) ;
      fclose ( $file_note ) ;

      // Defines container with notes
      echo '<div id="boxnote">' ;
      echo $note_data ;
      echo '</div>' ;
    }
    else {
      if ( $_POST["outform"] == 'txt' ) {
        echo '<pre> </pre>' ;
      }
    }

    /*
       Part depending on output Format
    */

    switch ($_POST["outform"]) {
     case 'html':
      /*
         In HTML, the output is a table
         whose columns depend on the
         requested quantities to display.
      */
      if ($result->num_rows > 0) {
        echo '<table id="wave">' ;

        // Header of table
        $i_col = 0 ; // running index for column of table
        echo '<tr>' ;
        $qty = "outwave";
        if ( isset($_POST[$qty]) ) {
          $i_col++ ;
          echo '<th class="deflt">'.$lab_qty[$qty]."</th>";
        }
        $qty = "outaik";
        if ( isset($_POST[$qty]) ) {
          $i_col++ ;
          echo '<th class="deflt">'.$lab_qty[$qty]."</th>";
        }
        $qty = "outfik";
        if ( isset($_POST[$qty]) ) {
          $i_col++ ;
          echo '<th class="deflt">'.$lab_qty[$qty]."</th>";
        }
        for ( $i=0; $i<2; $i++ ) {
          $qty = "outener" ;
          if ( isset($_POST[$qty]) ) {
            $i_col++ ;
            echo '<th class="deflt">'.$lab_lev[$i]." ".$lab_qty[$qty]."</th>";
          }
          $qty = "outgl" ;
          if ( isset($_POST[$qty]) ) {
            $i_col++ ;
            echo '<th class="lande">'.$lab_lev[$i]." ".$lab_qty[$qty]."</th>";
          }
          $qty = "outj" ;
          if ( isset($_POST[$qty]) ) {
            $i_col++ ;
            echo '<th class="lande">'.$lab_lev[$i]." ".$lab_qty[$qty]."</th>";
          }
          $qty = "outpar" ;
          if ( isset($_POST[$qty]) ) {
            $i_col++ ;
            echo '<th class="lande">'.$lab_lev[$i]." ".$lab_qty[$qty]."</th>";
          }
          $qty = "outconf" ;
          if ( isset($_POST[$qty]) ) {
            $i_col++ ;
            echo '<th class="deflt">'.$lab_lev[$i]." ".$lab_qty[$qty]."</th>";
          }
          $qty = "outterm" ;
          if ( isset($_POST[$qty]) ) {
            $i_col++ ;
            echo '<th class="term">'.$lab_lev[$i]." ".$lab_qty[$qty]."</th>";
            $qty = "outprct" ;
            $i_col++ ;
            echo '<th class="lande">'.$lab_lev[$i]." ".$lab_qty[$qty]."</th>";
          }
        }
        $nb_col_out = $i_col++ ; // Nber of output columns
        echo "</tr>" ;

        // output data of each row
        while ( $row = $result->fetch_assoc() ) {
          echo '<tr>' ;
          $qty = "outwave";
          if ( isset($_POST[$qty]) ) {
            print_exp_th_html ($row[$trs_wvl_exp],$row[$trs_wvl_th]) ;
          }
          $qty = "outaik";
          if ( isset($_POST[$qty]) ) {
            printf ("<td>%.3e</td>", $row[$trs_aki_th] ) ;
          }
          $qty = "outfik";
          if ( isset($_POST[$qty]) ) {
            printf ("<td>%.3e</td>", $row[$trs_fik_th] ) ;
          }
          for ( $i=0; $i<2; $i++ ) {
            $qty = "outener" ;
            if ( isset($_POST[$qty]) ) {
              print_exp_th_html ($row[$levs_in_trs[$i].$lev_enr_exp]
                               , $row[$levs_in_trs[$i].$lev_enr_th]) ;
            }
            $qty = "outgl" ;
            if ( isset($_POST[$qty]) ) {
              print_exp_th_html ($row[$levs_in_trs[$i].$lev_gl_exp]
                               , $row[$levs_in_trs[$i].$lev_gl_th]) ;
            }
            $qty = "outj" ;
            if ( isset($_POST[$qty]) ) {
              echo "<td>".print_j($row[$levs_in_trs[$i].$lev_j])."</td>" ;
            }
            $qty = "outpar" ;
            if ( isset($_POST[$qty]) ) {
              echo "<td>".$lab_par[$row[$levs_in_trs[$i].$lev_par]]."</td>" ;
            }
            $qty = "outconf" ;
            if ( isset($_POST[$qty]) ) {
              echo "<td>".$row[$cfgs_in_trs[$i].$cfg_nam_htm]."</td>" ;
            }
            $qty = "outterm" ;
            if ( isset($_POST[$qty]) ) {
              echo "<td>".$row[$levs_in_trs[$i].$lev_trm_htm]."</td>" ;
              $qty = "outprct" ;
              echo "<td>".$row[$levs_in_trs[$i].$lev_trm_prc]."</td>" ;
            }
          }
          echo "</tr>" ;
/*
          echo "<tr>" ;
          foreach ($row as $key => $val) {
            echo ("<tr>&nbsp;".$key." ".$val."</tr>") ;
          }
          echo "</tr>" ;
          echo "<tr><td>&nbsp;" . $row[$trs_wvl_exp] . "</td>"
           . "<td>&nbsp;" . $row[$trs_wvl_th]. "</td>"
           . "<td>&nbsp;" . $row[$trs_aki_th]. "</td>"
           . "<td>&nbsp;" . $row[$trs_fik_th]. "</td>"
           . "<td>&nbsp;" . $row[$levs_in_trs[0].$lev_enr_exp]. "</td>"
           . "<td>&nbsp;" . $row[$levs_in_trs[0].$lev_enr_th ]. "</td>"
           . "<td>&nbsp;" . $row[$levs_in_trs[0].$lev_gl_exp]. "</td>"
           . "<td>&nbsp;" . $row[$levs_in_trs[0].$lev_gl_th ]. "</td>"
           . "<td>&nbsp;" . $row[$cfgs_in_trs[0].$cfg_nam_htm]. "</td>"
           . "<td>&nbsp;" . $row[$levs_in_trs[0].$lev_trm_htm]. "</td>"
           . "<td>&nbsp;" . $row[$levs_in_trs[0].$lev_trm_prc]. "</td>"
           . "<td>&nbsp;" . $row[$levs_in_trs[1].$lev_enr_exp]. "</td>"
           . "<td>&nbsp;" . $row[$levs_in_trs[1].$lev_enr_th ]. "</td>"
           . "<td>&nbsp;" . $row[$levs_in_trs[1].$lev_gl_exp]. "</td>"
           . "<td>&nbsp;" . $row[$levs_in_trs[1].$lev_gl_th ]. "</td>"
           . "<td>&nbsp;" . $row[$cfgs_in_trs[1].$cfg_nam_htm]. "</td>"
           . "<td>&nbsp;" . $row[$levs_in_trs[1].$lev_trm_htm]. "</td>"
           . "<td>&nbsp;" . $row[$levs_in_trs[1].$lev_trm_prc]. "</td>"
          ;
*/
        }
        echo "</table>" ;
      }
      break;

     case 'txt':
      /*
         In text format, the output is a
         sequence of line with <pre> tag.
      */
      echo '<div id="outtxt">' ;
      if ($result->num_rows > 0) {

        // Header of table
        $i_col = 0 ; // running index for column of table
        $qty = "outwave";
        if ( isset($_POST[$qty]) ) {
          $i_col++ ;
          echo '<pre># Col. '.$i_col." : ".$lab_qty[$qty]."</pre>";
          $i_col++ ;
          echo '<pre># Col. '.$i_col." : E/T flag for exp/th quantity</pre>";
        }
        $qty = "outaik";
        if ( isset($_POST[$qty]) ) {
          $i_col++ ;
          echo '<pre># Col. '.$i_col." : ".$lab_qty[$qty]."</pre>";
        }
        $qty = "outfik";
        if ( isset($_POST[$qty]) ) {
          $i_col++ ;
          echo '<pre># Col. '.$i_col." : ".$lab_qty[$qty]."</pre>";
        }
        for ( $i=0; $i<2; $i++ ) {
          $qty = "outener" ;
          if ( isset($_POST[$qty]) ) {
            $i_col++ ;
            echo '<pre># Col. '.$i_col." : ".$lab_lev[$i]." ".$lab_qty[$qty]."</pre>";
            $i_col++ ;
            echo '<pre># Col. '.$i_col." : E/T flag for exp/th quantity</pre>";
          }
          $qty = "outgl" ;
          if ( isset($_POST[$qty]) ) {
            $i_col++ ;
            echo '<pre># Col. '.$i_col." : ".$lab_lev[$i]." ".$lab_qty[$qty]."</pre>";
            $i_col++ ;
            echo '<pre># Col. '.$i_col." : E/T flag for exp/th quantity</pre>";
          }
          $qty = "outj" ;
          if ( isset($_POST[$qty]) ) {
            $i_col++ ;
            echo '<pre># Col. '.$i_col." : ".$lab_lev[$i]." ".$lab_qty[$qty]."</pre>";
          }
          $qty = "outpar" ;
          if ( isset($_POST[$qty]) ) {
            $i_col++ ;
            echo '<pre># Col. '.$i_col." : ".$lab_lev[$i]." ".$lab_qty[$qty]."</pre>";
          }
          $qty = "outconf" ;
          if ( isset($_POST[$qty]) ) {
            $i_col++ ;
            echo '<pre># Col. '.$i_col." : ".$lab_lev[$i]." ".$lab_qty[$qty]." (32-character wide)</pre>";
          }
          $qty = "outterm" ;
          if ( isset($_POST[$qty]) ) {
            $i_col++ ;
            echo '<pre># Col. '.$i_col." : ".$lab_lev[$i]." ".$lab_qty[$qty]."</pre>";
            $qty = "outprct" ;
            $i_col++ ;
            echo '<pre># Col. '.$i_col." : ".$lab_lev[$i]." ".$lab_qty[$qty]."</pre>";
          }
        }
        echo '<pre>#</pre>';
        $nb_col_out = $i_col++ ; // Nber of output columns

        // output data of each row
        while ( $row = $result->fetch_assoc() ) {
          echo '<pre>' ;
          $qty = "outwave";
          if ( isset($_POST[$qty]) ) {
            print_exp_th_txt ( $form_qty[$qty]
                             , $row[$trs_wvl_exp], $row[$trs_wvl_th] ) ;
          }
          $qty = "outaik";
          if ( isset($_POST[$qty]) ) {
            printf ( $form_qty[$qty], $row[$trs_aki_th] ) ;
          }
          $qty = "outfik";
          if ( isset($_POST[$qty]) ) {
            printf ( $form_qty[$qty], $row[$trs_fik_th] ) ;
          }
          for ( $i=0; $i<2; $i++ ) {
            $qty = "outener" ;
            if ( isset($_POST[$qty]) ) {
              print_exp_th_txt ( $form_qty[$qty]
                               , $row[$levs_in_trs[$i].$lev_enr_exp]
                               , $row[$levs_in_trs[$i].$lev_enr_th]) ;
            }
            $qty = "outgl" ;
            if ( isset($_POST[$qty]) ) {
              print_exp_th_txt ( $form_qty[$qty]
                               , $row[$levs_in_trs[$i].$lev_gl_exp]
                               , $row[$levs_in_trs[$i].$lev_gl_th]) ;
            }
            $qty = "outj" ;
            if ( isset($_POST[$qty]) ) {
              printf ($form_qty[$qty], $row[$levs_in_trs[$i].$lev_j] ) ;
            }
            $qty = "outpar" ;
            if ( isset($_POST[$qty]) ) {
              printf ($form_qty[$qty], $lab_par[$row[$levs_in_trs[$i].$lev_par]] ) ;
            }
            $qty = "outconf" ;
            if ( isset($_POST[$qty]) ) {
              printf ($form_qty[$qty], $row[$cfgs_in_trs[$i].$cfg_nam_txt] ) ;
            }
            $qty = "outterm" ;
            if ( isset($_POST[$qty]) ) {
              printf ( $form_qty[$qty], $row[$levs_in_trs[$i].$lev_trm_txt] ) ;
//              printf ( $form_qty[$qty], $row[$levs_in_trs[$i].$lev_trm_txt] ) ;
              $qty = "outprct" ;
              printf ( $form_qty[$qty], $row[$levs_in_trs[$i].$lev_trm_prc] ) ;
            }
          }
          echo '</pre>' ;
        }
      }
      echo '</div>' ;
      break;
     default:
      die (" ERROR in output format !") ;
      break;
    }
    $conn->close() ;
  ?>
</body>
</html>
